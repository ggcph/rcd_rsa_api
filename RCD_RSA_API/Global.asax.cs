﻿using System.Web;
using System.Web.Http;

namespace RCD_RSA_API
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}

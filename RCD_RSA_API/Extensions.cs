﻿
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace RCD_RSA_API
{
    public static class Extensions
    {

        public static IList tailSet(this IList list, IComparable fromElement)
        {
            IList result;
            for (int i = 0; i < list.Count; i++)
            {
                bool flag = fromElement.CompareTo(list[i]) <= 0;
                if (flag)
                {
                    ArrayList arrayList = new ArrayList();
                    for (int j = i; j < list.Count; j++)
                    {
                        arrayList.Add(list[j]);
                    }
                    result = arrayList;
                    return result;
                }
            }
            result = new ArrayList();
            return result;
        }

        public static void nextBytes(this Random random, byte[] array)
        {
            random.NextBytes(array);
        }

        public static object ceilingKey(this SortedList sortedList, IComparable key)
        {
            object result;
            foreach (object current in sortedList.GetKeyList())
            {
                bool flag = key.CompareTo(current) <= 0;
                if (flag)
                {
                    result = current;
                    return result;
                }
            }
            result = null;
            return result;
        }

        public static void clear(this SortedList sortedList)
        {
            sortedList.Clear();
        }

        public static IList navigableKeySet(this SortedList sortedList)
        {
            return sortedList.GetKeyList();
        }

        public static void close(this StreamWriter writer)
        {
            writer.Close();
        }

        public static void flush(this StreamWriter writer)
        {
            writer.Flush();
        }

        public static void write(this StreamWriter writer, string value)
        {
            writer.Write(value);
        }

        public static bool contains(this string str, string value)
        {
            return str.Contains(value);
        }

        public static bool endsWith(this string str, string value)
        {
            return str.EndsWith(value);
        }

        public static bool equals(this string str, string value)
        {
            return str.Equals(value);
        }

        public static int indexOf(this string str, char value)
        {
            return str.IndexOf(value);
        }

        public static int indexOf(this string str, char value, int startIndex)
        {
            return str.IndexOf(value, startIndex);
        }

        public static bool regionMatches(this string str, int thisOffset, string other, int otherOffset, int length)
        {
            bool flag = thisOffset + length > str.Length || otherOffset + length > other.Length;
            return !flag && str.Substring(thisOffset, length) == other.Substring(otherOffset, length);
        }

        public static string replace(this string str, string oldValue, string newValue)
        {
            return str.Replace(oldValue, newValue);
        }

        public static string replace(this string str, char oldValue, char newValue)
        {
            return str.Replace(oldValue, newValue);
        }

        public static string[] split(this string str, string regex)
        {
            return Regex.Split(str, regex);
        }

        public static string trim(this string str)
        {
            return str.Trim();
        }

        public static StringBuilder append(this StringBuilder builder, char value)
        {
            builder.Append(value);
            return builder;
        }

        public static StringBuilder append(this StringBuilder builder, int value)
        {
            builder.Append(value);
            return builder;
        }

        public static StringBuilder append(this StringBuilder builder, long value)
        {
            builder.Append(value);
            return builder;
        }

        public static StringBuilder append(this StringBuilder builder, string value)
        {
            builder.Append(value);
            return builder;
        }

        public static string toString(this StringBuilder builder)
        {
            return builder.ToString();
        }

        public static void close(this TextReader reader)
        {
            reader.Close();
        }

        public static string readLine(this TextReader reader)
        {
            return reader.ReadLine();
        }
    }
}
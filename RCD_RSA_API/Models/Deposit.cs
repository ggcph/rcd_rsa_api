﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RCD_RSA_API.Models.Deposit
{

    [XmlRoot(ElementName = "billInfoReport")]
    public class BillInfoReport
    {
        [XmlElement(ElementName = "bnType")]
        public string BnType { get; set; }
        [XmlElement(ElementName = "denomination")]
        public string Denomination { get; set; }
        [XmlElement(ElementName = "bnNumber")]
        public string BnNumber { get; set; }
    }

    [XmlRoot(ElementName = "billInfoReports")]
    public class BillInfoReports
    {
        [XmlElement(ElementName = "billInfoReport")]
        public List<BillInfoReport> BillInfoReport { get; set; }
    }

    [XmlRoot(ElementName = "parameter")]
    public class Parameter
    {
        [XmlElement(ElementName = "serialRecordId")]
        public string SerialRecordId { get; set; }
        [XmlElement(ElementName = "bussinessNo")]
        public string BussinessNo { get; set; }
        [XmlElement(ElementName = "devId")]
        public string DevId { get; set; }
        [XmlElement(ElementName = "date")]
        public string Date { get; set; }
        [XmlElement(ElementName = "company")]
        public string Company { get; set; }
        [XmlElement(ElementName = "department")]
        public string Department { get; set; }
        [XmlElement(ElementName = "depositType")]
        public string DepositType { get; set; }
        [XmlElement(ElementName = "sondepositType")]
        public string SondepositType { get; set; }
        [XmlElement(ElementName = "discountType")]
        public string DiscountType { get; set; }
        [XmlElement(ElementName = "discount")]
        public string Discount { get; set; }
        [XmlElement(ElementName = "discountremarks")]
        public string Discountremarks { get; set; }
        [XmlElement(ElementName = "time")]
        public string Time { get; set; }
        [XmlElement(ElementName = "userId")]
        public string UserId { get; set; }
        [XmlElement(ElementName = "classNum")]
        public string ClassNum { get; set; }
        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; }
        [XmlElement(ElementName = "billCount")]
        public string BillCount { get; set; }
        [XmlElement(ElementName = "billValue")]
        public string BillValue { get; set; }
        [XmlElement(ElementName = "coinCount")]
        public string CoinCount { get; set; }
        [XmlElement(ElementName = "coinTotalValue")]
        public string CoinTotalValue { get; set; }
        [XmlElement(ElementName = "enveTotalValue")]
        public string EnveTotalValue { get; set; }
        [XmlElement(ElementName = "bussinessType")]
        public string BussinessType { get; set; }
        [XmlElement(ElementName = "bussinessItem")]
        public string BussinessItem { get; set; }
        [XmlElement(ElementName = "extraInfo")]
        public string ExtraInfo { get; set; }
        [XmlElement(ElementName = "billInfoReports")]
        public BillInfoReports BillInfoReports { get; set; }
        [XmlElement(ElementName = "coinInfoReports")]
        public string CoinInfoReports { get; set; }
        [XmlElement(ElementName = "otherInfoReports")]
        public string OtherInfoReports { get; set; }
    }

    [XmlRoot(ElementName = "root")]
    public class Root
    {

        [XmlElement(ElementName = "parameter")]
        public Parameter Parameter { get; set; }
    }
}
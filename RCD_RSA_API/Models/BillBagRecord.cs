﻿using System.Xml.Serialization;
namespace RCD_RSA_API.Models.BillBagRecord
{
    [XmlRoot(ElementName = "parameter")]
    public class Parameter
    {
        [XmlElement(ElementName = "serialRecordId")]
        public string SerialRecordId { get; set; }
        [XmlElement(ElementName = "devId")]
        public string DevId { get; set; }
        [XmlElement(ElementName = "startDate")]
        public string StartDate { get; set; }
        [XmlElement(ElementName = "startTime")]
        public string StartTime { get; set; }
        [XmlElement(ElementName = "endDate")]
        public string EndDate { get; set; }
        [XmlElement(ElementName = "endTime")]
        public string EndTime { get; set; }
        [XmlElement(ElementName = "username")]
        public string Username { get; set; }
        [XmlElement(ElementName = "preBagNum")]
        public string PreBagNum { get; set; }
        [XmlElement(ElementName = "curBagNum")]
        public string CurBagNum { get; set; }
        [XmlElement(ElementName = "preCoinBagNum")]
        public string PreCoinBagNum { get; set; }
        [XmlElement(ElementName = "curCoinBagNum")]
        public string CurCoinBagNum { get; set; }
        [XmlElement(ElementName = "oldBatchNum")]
        public string OldBatchNum { get; set; }
        [XmlElement(ElementName = "newBatchNum")]
        public string NewBatchNum { get; set; }
        [XmlElement(ElementName = "depositNumber")]
        public string DepositNumber { get; set; }
    }

    
    [XmlRoot(ElementName = "root")]
    public class Root
    {
        [XmlElement(ElementName = "parameter")]
        public Parameter Parameter { get; set; }
    }

}

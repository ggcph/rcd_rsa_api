﻿using java.io;
using java.lang;
using java.math;
using java.security;
using java.security.interfaces;
using java.security.spec;
using javax.crypto;
using System;
using System.Collections.Generic;


namespace RCD_RSA_API
{
    public class RSAUtils
    {
        /*
        static void Main(string[] args)
        {
            string data = "b33dde3a-19b8-42e7-8df2-ae33486fa92b@Scc+64L5+ZMi5DuDjYRV4/mqfPoyBaLLKfs+dHqw3r6AeLuEc2tJFgb1msmmv31BPMWJ67T4+5ImplyQG0cmvI2918BaNVQNgWV/RLqad/VTyWs0U6UZ6FPhFCOaZPswIBjMzI0ns2O2gmuNaBKxsldL6XxWDzC+niB3R+XfAowHKQ1m841jKKWa6TqguP7c4oh4yt9tnAjwmLyNpwNl7+P0ATmF/iipunDN+FDMQHtnfySPB/jpsrbHKxRNiR5JX+eW4zN/NNubW/RBG4ZhgpxrP1Mcbm5la6w2LnGYne1bbNfOrcQdFwclxRteVAXY6AWs9zNa8KUzvjvxbu3CkwAzjZGPn3I+qrajEU02a8wD85razQNSIsBvNXk9iGoLc5V9nLoyJmK1MkUNTBTT2MxLN8j9ERHpOzRNTbe/N2Taab0XTaoxH6dhrAmko+ip/XN1ZOzARhAp+oNiLf6/ne7wWAP5FSs/dZZaoU/lhuJM9PWQXqtEfOXVOmLenyLTERJjQs/aKii+w1XD0ENEWv5GB7EOPqvBMDhAUjrk4/QGkl/onM9eCMxyULlDJo5Qd5Im9514vRKXWa8w2486lZS6XLNRzsst5RgpVViOcEzQl8+R/BWOxmEU4bcjA5GoRc58XRHzvWpPLHkGz9fi8bUeeEkFL9Kpb7Bv1QniI3BFqzdTtkZP2NMGSMZo0iPd9q3Q3koNV7SGC8RB5xjIukOAs4ocThBUlKrvEpBQ5vFdY7aGKU7HN0feGMotc71rOO2Oi1rvbzxL29XWKfIUgpClxhUynqWsHeBf93W3RtusbyjhcnU3EQv0Fh6etb9P7fnRY74mzB2p4dsRXVH7VSCkdyqC6xP+q0yJRm4qRiXDGKlrvppC7yaA+JmtdWvArfZn8Wqt/dEhq3SAm48pWGbIZs66pO5iYkJJXzgfSQVFULg8xK87o1UO8cmsc6MQqPfq8Pe5x2Ll3v8xpX4ZNMBtD3Cc0FuunvEiyuA6ysh/sjNHgwo7Z2VHXh7G/kMri5WiyLlJpZhTyfGLS1X2ctJq2O2VYAw5FeYui0q0OtT2H/3fAF5Nv0j9zK88Hh7SHmH6axIwQbkMXULJ0qns9HFETPfe/pjPjkmv9xnqmDCfm4z9iOaA2HsR3Ka8iJ52BPNzqADPWP0ke+2XT4AqUM5rezW90w6EJzN3Sn9o2oMdl3l5r4bbQ2BQXjkE4iMrVGPr5r1OL9t5FcRcLNbFAO0vcfwPv3HYtoloYfJaM8vca83evLDv2mPRZH01Yex+mlDtLUn3AtuYnsUFdfCqhN3Esn8zHs1jphD66H2OzuDvBZAod8MPRdJk32bw9hAoRLDmKYbhzhjpR3hkUO3yCH1X2puATEb8l2D2a8rVG9gwLxkD3xMMBT+COZZVB4QIX4uadlrqeHQ3sbcmKFGObHBb+yuyjqG99hsHkvKzU658ZYQ4XEXrwELbhNo0bziq/EdxkLoYjlX/kRui4IvjJ90l0EpAkXLAeyK0vPh2D57F3kZAZy0/hXpL2QgQ1T6bNTc07terihLwScuxW0a98goRaI/mXZF3Gq/ZrcJ33fycu6Ke+8PhOCsREupcYrQDqrbxw6+f7XOxdBnNGtemBLp9Zxl2NDx8BIfMQWtYgfyF1igZlfhSK1MTJlCh9a0lOkXMVWXzvS7OdFpv+QCM1DYi5ftXb8HFyoIgkdQTWMYfUXIibMMGO9nLBu8WjsUxFOTiiFZxu71J5Cg5R7oj9r+R0F27aoPo1tDTHLGpZCnA9DoNC864gxTyLFyONzHa5MDZNrXuZb23m+8LR1GRvCDUHXGCxH9O2ccqdQMIgwd9qtkCEVaLkJK+h1eutX8zzh4HK7wPHAASPBfwkWxHjZNOI76bZJyKGTrOhKNsCi8kxneKdc2HLd966uu8GdveuDjf4Re52qFaV5foNGaGdge1SsWWSxKeMjmIHLkyXjteB/HfomZ+zllUHHGUCyVHqKba3k3sekxJlnr1nGfw4Xd4tSuN4/RWJr+0mXIawQ7yrxLa5Pi5+UowpJoVWXutTW7ra4lbyixLiz2dA7xbP92UfYX7DUAqpeYB+5eBmQyK2pqUIXtL4BTdG86MOXu/8biz8HGLfTFcwMaEiott9sbIQdkTgHBQ66toxz2oGHupFiyKkVMiQVLbB9pgxG+5eksQ/MGsovNkrCND8+IyasdBppjzzNVu1TtoepI27pU=";

            string returnData = "";
            //String[] strlist = str.Split(spearator, count,
            //   StringSplitOptions.RemoveEmptyEntries);
            string devId = data.split("@")[0];
            string encdParam = data.Substring(devId.Length + 1);

            string PrivateKey = "MIIBNgIBADANBgkqhkiG9w0BAQEFAASCASAwggEcAgEAAoGBAJke2qXHj9w1ZOa0wA2DevURphZcSzIMCTFokp+jcNgp4YKoaZyg3eackzPzGx5Q7t+M+0NaAUhCrsQKGGopVbyTLipWSFQzWtZSxkJWrxxX9C36Zx859M2X0AY6/BcNNdtxKaFfIj3TkMJeihL76+hyNUOIXITcP/rjSBOLy7UJAgEAAoGACDgtyIRj5pA/MFs3jAIo0Yr2/XBK4Q7gV3SjmkotvozQ/DCKKsw/3Sc+h6VfKUty5zEsM7tbIkPiGcfo85nwbi+4OnfPqm0zqnep44Og/U+WFVYx7tiUJQkDk0rOUoDCUYQaROjDOizEiq4Y+ynpB4EL1RylHNUmbRu2z2H5TmECAQACAQACAQACAQACAQA=";

            RSAPrivateKey p = GetPrivateKey(PrivateKey);

            encdParam = PrivateDecrypt(encdParam, GetPrivateKey(PrivateKey)).Trim();
        }
        */
        public string PrivateKey = "MIIBNgIBADANBgkqhkiG9w0BAQEFAASCASAwggEcAgEAAoGBAJke2qXHj9w1ZOa0wA2DevURphZcSzIMCTFokp+jcNgp4YKoaZyg3eackzPzGx5Q7t+M+0NaAUhCrsQKGGopVbyTLipWSFQzWtZSxkJWrxxX9C36Zx859M2X0AY6/BcNNdtxKaFfIj3TkMJeihL76+hyNUOIXITcP/rjSBOLy7UJAgEAAoGACDgtyIRj5pA/MFs3jAIo0Yr2/XBK4Q7gV3SjmkotvozQ/DCKKsw/3Sc+h6VfKUty5zEsM7tbIkPiGcfo85nwbi+4OnfPqm0zqnep44Og/U+WFVYx7tiUJQkDk0rOUoDCUYQaROjDOizEiq4Y+ynpB4EL1RylHNUmbRu2z2H5TmECAQACAQACAQACAQACAQA=";
        public string PublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCZHtqlx4/cNWTmtMANg3r1EaYWXEsyDAkxaJKfo3DYKeGCqGmcoN3mnJMz8xseUO7fjPtDWgFIQq7EChhqKVW8ky4qVkhUM1rWUsZCVq8cV/Qt+mcfOfTNl9AGOvwXDTXbcSmhXyI905DCXooS++vocjVDiFyE3D/640gTi8u1CQIDAQAB";

        public string CHARSET = "UTF-8";
        public string RSA_ALGORITHM = "RSA";


        public IDictionary<string, string> createKeys(int keySize)
        {
            KeyPairGenerator kpg;
            try
            {
                kpg = KeyPairGenerator.getInstance("RSA");
            }
            catch (NoSuchAlgorithmException)
            {
                throw new System.ArgumentException("No such algorithm-->[RSA]");
            }

            kpg.initialize(keySize);

            KeyPair keyPair = kpg.generateKeyPair();

            Key publicKey = keyPair.getPublic();
            string publicKeyStr = Convert.ToBase64String(publicKey.getEncoded());

            Key privateKey = keyPair.getPrivate();
            string privateKeyStr = Convert.ToBase64String(privateKey.getEncoded());
            IDictionary<string, string> keyPairMap = new Dictionary<string, string>();
            keyPairMap["publicKey"] = publicKeyStr;
            keyPairMap["privateKey"] = privateKeyStr;

            return keyPairMap;
        }


        public PublicKey getPublicKey(byte[] keyBytes)
        {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        }

        public PublicKey getPublicKey(string modulus, string publicExponent)
        {
            BigInteger bigIntModulus = new BigInteger(modulus);
            BigInteger bigIntPrivateExponent = new BigInteger(publicExponent);
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(bigIntModulus, bigIntPrivateExponent);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        }


        //use this
        public RSAPublicKey getPublicKey(string publicKey)
        {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Convert.FromBase64String(publicKey));
            return (RSAPublicKey)keyFactory.generatePublic(x509KeySpec);
        }

        public PrivateKey getPrivateKey(byte[] keyBytes)
        {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePrivate(keySpec);
        }

        public PrivateKey getPrivateKey(string modulus, string privateExponent)
        {
            BigInteger bigIntModulus = new BigInteger(modulus);
            BigInteger bigIntPrivateExponent = new BigInteger(privateExponent);
            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(bigIntModulus, bigIntPrivateExponent);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePrivate(keySpec);
        }

        //use this
        public RSAPrivateKey GetPrivateKey(string privateKey)
        {
            try
            {
                java.security.KeyFactory keyFactory = java.security.KeyFactory.getInstance("RSA");
                java.security.spec.PKCS8EncodedKeySpec pkcs8KeySpec = new java.security.spec.PKCS8EncodedKeySpec(Convert.FromBase64String(privateKey));
                return (RSAPrivateKey)keyFactory.generatePrivate(pkcs8KeySpec);
            }
            catch (java.lang.Exception ex)
            {
                throw ex;
            }

        }

        //use this
        public string publicEncrypt(string data, RSAPublicKey publicKey)
        {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(1, publicKey);
            return Convert.ToBase64String(rsaSplitCodec(cipher, 1, (byte[])(Array)data.GetBytes("UTF-8"), publicKey.getModulus().bitLength()));
        }

        //use this
        public string PrivateEncrypt(string data, RSAPrivateKey privateKey)
        {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(1, privateKey);
            return Convert.ToBase64String(rsaSplitCodec(cipher, 1, (byte[])(Array)data.GetBytes("UTF-8"), privateKey.getModulus().bitLength()));
        }

        //use this
        public string PrivateDecrypt(string data, RSAPrivateKey privateKey)
        {
            javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(2, privateKey);
            byte[] codec = rsaSplitCodec(cipher, 2, Convert.FromBase64String(data), privateKey.getModulus().bitLength());

            return StringHelper.NewString((sbyte[])(Array)codec, "UTF-8");
        }

        public string publicDecrypt(string data, RSAPublicKey publicKey)
        {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(2, publicKey);

            byte[] codec = rsaSplitCodec(cipher, 2, Convert.FromBase64String(data), publicKey.getModulus().bitLength());
            return StringHelper.NewString((sbyte[])(Array)codec, "UTF-8");

            //return new string(rsaSplitCodec(cipher, 2, Convert.FromBase64String(data), publicKey.getModulus().bitLength()), "UTF-8");
        }




        public byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] datas, int keySize)
        {
            int maxBlock = 0;
            if (opmode == 2)
            {
                maxBlock = keySize / 8;
            }
            else
            {
                maxBlock = keySize / 8 - 11;
            }
            ByteArrayOutputStream @out = new ByteArrayOutputStream();
            int offSet = 0;

            int i = 0;
            try
            {
                while (datas.Length > offSet)
                {
                    byte[] buff; if (datas.Length - offSet > maxBlock)
                    {
                        buff = cipher.doFinal(datas, offSet, maxBlock);
                    }
                    else
                    {
                        buff = cipher.doFinal(datas, offSet, datas.Length - offSet);
                    }
                    @out.write(buff, 0, buff.Length);
                    i++;
                    offSet = i * maxBlock;
                }
            }
            catch (java.lang.Exception e)
            {
                throw new RuntimeException("������������������[" + maxBlock + "]������������������������", e);
            }
            byte[] resultDatas = @out.toByteArray();
            //closeQuietly(@out);
            @out.close();
            return resultDatas;
        }

    }

}

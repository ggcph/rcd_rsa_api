﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using RCD_RSA_API.Models.Deposit;
using RCD_RSA_API.Models.BillBagRecord;
using System.Text;
using Newtonsoft.Json;
using com.snbc.WebService.dto;
using System.Web.Configuration;
using java.security.interfaces;

namespace RCD_RSA_API
{
    /// <summary>
    /// Summary description for RCDSecurity
    /// </summary>
    [WebService(Namespace = "http://ggc.ph/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

   
    public class RCDSecurity : System.Web.Services.WebService
    {
        HttpRuntimeSection httpRuntimeSection = new HttpRuntimeSection();
        
        static string publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCZHtqlx4/cNWTmtMANg3r1EaYWXEsyDAkxaJKfo3DYKeGCqGmcoN3mnJMz8xseUO7fjPtDWgFIQq7EChhqKVW8ky4qVkhUM1rWUsZCVq8cV/Qt+mcfOfTNl9AGOvwXDTXbcSmhXyI905DCXooS++vocjVDiFyE3D/640gTi8u1CQIDAQAB";
        static string privateKey = "MIIBNgIBADANBgkqhkiG9w0BAQEFAASCASAwggEcAgEAAoGBAJke2qXHj9w1ZOa0wA2DevURphZcSzIMCTFokp+jcNgp4YKoaZyg3eackzPzGx5Q7t+M+0NaAUhCrsQKGGopVbyTLipWSFQzWtZSxkJWrxxX9C36Zx859M2X0AY6/BcNNdtxKaFfIj3TkMJeihL76+hyNUOIXITcP/rjSBOLy7UJAgEAAoGACDgtyIRj5pA/MFs3jAIo0Yr2/XBK4Q7gV3SjmkotvozQ/DCKKsw/3Sc+h6VfKUty5zEsM7tbIkPiGcfo85nwbi+4OnfPqm0zqnep44Og/U+WFVYx7tiUJQkDk0rOUoDCUYQaROjDOizEiq4Y+ynpB4EL1RylHNUmbRu2z2H5TmECAQACAQACAQACAQACAQA=";

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string DecryptDepositData(string Parameter)
        {
            
            try
            {
                string devId = Parameter.split("@")[0];
                string encdParam = Parameter.Substring(devId.Length + 1);

                RSAUtils rsaUtils = new RSAUtils();
                encdParam = rsaUtils.PrivateDecrypt(encdParam, rsaUtils.GetPrivateKey(privateKey.ToString())).Trim();

                //encdParam = RemoveTroublesomeCharacters(encdParam);
                //encdParam = encdParam.Replace("root", "deposit");
                //XmlSerializer serializer = new XmlSerializer(typeof(Models.Deposit.Deposit), new XmlRootAttribute("deposit"));
                //StringReader stringReader = new StringReader(encdParam);
                //Models.Deposit.Deposit root = (Models.Deposit.Deposit)serializer.Deserialize(stringReader);

                return encdParam;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public string DecryptBillBagData(string Parameter)
        {
            
            try
            {
                string encdParam = Parameter; 

                RSAUtils rsaUtils = new RSAUtils();
                encdParam = rsaUtils.PrivateDecrypt(encdParam, rsaUtils.GetPrivateKey(privateKey.ToString())).Trim();
                
                //encdParam = RemoveTroublesomeCharacters(encdParam);
                //encdParam = encdParam.Replace("root", "billBagRecord");
                //XmlSerializer serializer = new XmlSerializer(typeof(Models.BillBagRecord.BillBagRecord), new XmlRootAttribute("billBagRecord"));
                //StringReader stringReader = new StringReader(encdParam);
                //Models.BillBagRecord.BillBagRecord root = (Models.BillBagRecord.BillBagRecord)serializer.Deserialize(stringReader);

                return encdParam;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        [WebMethod]
        public string DecryptPublicBillBagData(string Parameter)
        {

            try
            {
                string encdParam = Parameter;

                RSAUtils rsaUtils = new RSAUtils();
                //encdParam = rsaUtils.PrivateDecrypt(encdParam, rsaUtils.GetPrivateKey(privateKey.ToString())).Trim();

                RSAPublicKey rSAPublicKey = rsaUtils.getPublicKey(publicKey.ToString());
                encdParam = rsaUtils.publicDecrypt(encdParam, rSAPublicKey);

               // encdParam = RemoveTroublesomeCharacters(encdParam);
                //encdParam = encdParam.Replace("root", "billBagRecord");
                //XmlSerializer serializer = new XmlSerializer(typeof(Models.BillBagRecord.BillBagRecord), new XmlRootAttribute("billBagRecord"));
                //StringReader stringReader = new StringReader(encdParam);
                //Models.BillBagRecord.BillBagRecord root = (Models.BillBagRecord.BillBagRecord)serializer.Deserialize(stringReader);

                return encdParam;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        [WebMethod]
        public string EncryptMessage(string Parameter)
        {
            string encodedData = "";
            RSAUtils rsaUtils = new RSAUtils();
            encodedData = rsaUtils.PrivateEncrypt(Parameter, rsaUtils.GetPrivateKey(privateKey));
            return encodedData;
        }



        public static string RemoveTroublesomeCharacters(string inString)
        {
            if (inString == null) return null;

            StringBuilder newString = new StringBuilder();
            char ch;

            for (int i = 0; i < inString.Length; i++)
            {

                ch = inString[i];
                // remove any characters outside the valid UTF-8 range as well as all control characters
                // except tabs and new lines
                if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
                {
                    newString.Append(ch);
                }
            }
            return newString.ToString();

        }
    }
}
